/*
* Custom Scripts
*/


// --- Variables ---
var $header  = $('.site__header' ),
    $footer  = $('.site__footer' ),
    $content = $('.site__content'),
    $menu    = $header.find('.site__menu');


//-- Some Super Global Functions
function scrollHeight() {
  if ($(window).scrollTop() >= 1) {
    $header.addClass('skin--light');
  } else {
    $header.removeClass('skin--light');
  }
};


// Checking Site Scroll on Page Load
$(window).on('load', scrollHeight());




//-- When Page Ready...
jQuery(document).ready(function($) {
	'use strict';


	//-- General

	// Disable default link behavior for dummy links that have href='#'
  var $emptyLink = $('a[href="#"]');
  $emptyLink.on('click', function (e) {
    e.preventDefault();
  });


  // Animate.js
  $.fn.animated = function (inEffect) {
    $(this).each(function () {
      var ths = $(this);
      ths.css("opacity", "0").addClass("animated").waypoint(function (dir) {
        if (dir === "down") {
          ths.addClass(inEffect).css("opacity", "1");
        };
      }, {
        offset: "90%"
      });
    });
	};
	// Animate Asset
  $('.s__tarif-plan__inner').animated('fadeInDown');
  $('.site__footer__inner').animated('fadeInDown');



  // Adding Header class on scroll
  $(window).scroll(function () {
    scrollHeight();
  });


  // Site Menu Toggle
  $(function() {
    $('#menu--toggle').click(function () {
      $(this).toggleClass('is-active');
      $menu.toggleClass('menu--toggle');
    })

    $header.find('.site__menu-list a').click(function () {
      $('#menu--toggle').toggleClass('is-active');
      $menu.toggleClass('menu--toggle');
    });
	});


	//-- Local
  // ...

  // Tooltip init
  $('.checkout-tooltip').tooltip();

  // Add active class on click
  $('.s__tarif-plan__list-item').click(function() {

    $('.s__tarif-plan__list-item').removeClass('is-active');
    $(this).addClass('is-active');

  });


});/*Document Ready End*/
